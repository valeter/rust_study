extern crate rand;

use rand::Rng;

fn main() {
    let mut a = [0, 4564, 9, -23, 23894, 12, 7, 5, -3, 5, 89, 13, 57, 9, 22, 34, 57, 0, 1, 11];
    println!("before: {:?}", a);
    let from = 0;
    let to = a.len() - 1;
    quicksort(&mut a, from, to);
    println!("after:  {:?}", a);
}

const QUICKSORT_MIN_THRESHOLD: usize = 10;

fn quicksort(mut a: &mut [i32], low: usize, hi: usize) {
    if low >= hi {
        return;
    }
    if hi - low < QUICKSORT_MIN_THRESHOLD {
        insertion_sort(&mut a, low, hi);
        return;
    }

    let mid: usize = partition(&mut a, low, hi);
    let lw: isize = mid as isize - low as isize;
    let hw: isize = hi as isize - mid as isize;
    if mid < hi && hw < lw {
        quicksort(&mut a, mid, hi);
    }
    if (low as isize) < mid as isize - 1 {
        quicksort(&mut a, low, mid - 1);
    } 
    if mid < hi && hw >= lw {
        quicksort(&mut a, mid, hi);
    }
}

fn partition(mut a: &mut [i32], low: usize, hi: usize) -> usize {
    if low >= hi {
        return low;
    }
    let mid = fast_median(a, low, hi);
    let pivot = a[mid];
    let mut i: isize = low as isize;
    let mut j: isize = hi as isize;
    while i <= j {
        while a[i as usize] < pivot {
            i = i + 1;
        }
        while a[j as usize] > pivot {
            j = j - 1;
        }
        if i <= j {
            swap(&mut a, i as usize, j as usize);
            i = i + 1;
            j = j - 1;
        }
    }
    i as usize
}

fn fast_median(a: &[i32], low: usize, hi: usize) -> usize {
    let med1 = rand::thread_rng().gen_range(low, hi + 1);
    let med2 = rand::thread_rng().gen_range(low, hi + 1);
    let med3 = rand::thread_rng().gen_range(low, hi + 1);
    if (a[med1] >= a[med2] && a[med1] <= a[med3]) || (a[med1] >= a[med3] && a[med1] <= a[med2]) {
        return med1
    } else if (a[med2] >= a[med1] && a[med2] <= a[med3]) || (a[med2] >= a[med3] && a[med2] <= a[med1]) {
        return med2
    }
    med3
}

fn swap(mut a: &mut [i32], i1: usize, i2: usize) {
    let tmp = a[i1];
    a[i1] = a[i2];
    a[i2] = tmp;
}

fn insertion_sort(mut a: &mut [i32], low: usize, hi: usize) {
    if low >= hi {
        return;
    }
    for i in low+1..hi+1 {
        let temp = a[i];
        let mut j: isize = i as isize - 1;
        while j >= 0 && a[j as usize] > temp {
            a[(j + 1) as usize] = a[j as usize];
            j = j - 1;
        }
        a[(j + 1) as usize] = temp;
    }
}

fn selection_sort(mut a: &mut [i32], low: usize, hi: usize) {
    if low >= hi {
        return;
    }
    for i in low..hi+1 {
        for j in i+1..hi+1 {
            if a[i] > a[j] {
                swap(a, i, j);
            }
        }
    }
}